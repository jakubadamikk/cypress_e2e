# Description
1. Download and install <a href="https://nodejs.org/en/download/">node.js</a>
2. Download and install <a href="https://www.google.com/chrome/">Chrome browser</a>
2. Open project in IDE
3. Run ```npm install```
4. Run ```npx cypress run --browser chrome```  to run test directly from terminal

Alternatively tests can be run from test runner, to start in run ```npx cypress open```

NOTE: Running cypress 3.4.0/3.4.1 from terminal with node.js v12.11.0 and higer will return ENOTCONN error after finishing run due to <a href="https://github.com/cypress-io/cypress/issues/5241">Cypress bug</a>.

#Generating reports
1. After finished tests run ```npx mochawesome-merge --reportDir cypress/results > report.json```
2. Then ```npx mochawesome-report-generator report.json```
3. Result will be placed in ```mochawesome-report/report.html```