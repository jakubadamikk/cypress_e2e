describe('Login test set', function () {
    beforeEach(function () {
        cy.clearLocalStorage();
        cy.clearCookies();
        cy.visit('/login');
    });

    it('Login with valid data', function() {
        cy.server().route('POST', '/1/authorization/session').as('authorization');
        cy.login('validUser');
        cy.confirmSuccessfulLogin();
    });

    it('Login with invalid password', function() {
        cy.login('invalidUserShortPass');
        cy.confirmFailedLogin();
        cy.login('invalidUserWrongPass');
        cy.confirmFailedLogin();
    })
});