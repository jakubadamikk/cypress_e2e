Cypress.Commands.add('login', (userType, options = {}) => {
    const types = {
        validUser: {
            type: 'validUser'
        },
        invalidUserShortPass: {
            type: 'invalidUserShort'
        },
        invalidUserWrongPass: {
            type: 'invalidUserWrong'
        }
    };
    const user = types[userType];
    cy.fixture(user.type).as('user');
    cy.fixture('elements').as('e');
    cy.get('@e').then(e=>{
        cy.get('@user').then(user => {
            cy.get(e.emailField).clear();
            cy.get(e.emailField).type(user.email);
            cy.get(e.passwordField).clear();
            cy.get(e.passwordField).type(user.password);
        });
        cy.get(e.submitButton).click();
    })
});

Cypress.Commands.add('confirmSuccessfulLogin', () => {
    cy.fixture('validUser').as('user');
    cy.wait('@authorization').its('status').should('be', 204);
    cy.getCookie('token').should('exist');
    cy.get('@user').then(user => {
        cy.url().should('eq', user.dashboardUrl)
    })
});

Cypress.Commands.add('confirmFailedLogin', () => {
    cy.fixture('cssValues').as('style');
    cy.fixture('elements').as('e');
    cy.get('@e').then(e=>{
        cy.get('@style').then(style=> {
            cy.get(e.passwordField).should('have.css', 'background', style.highlightedField);
            cy.get(e.errorMessage).should('exist', 'be.visible').should('have.css', 'background', style.errorMessage);
        })
    });
    cy.getCookie('token').should('not.exist');
    cy.get('@user').then(user => {
        cy.url().should('eq',user.loginUrl)
    })
});